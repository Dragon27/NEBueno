<?php

namespace App\Http\Controllers;

use Iluminate\Http\Request;

use App\Http\Requests;

use App\Product;

//use App\Http\Controllers\ShoppingCart;

class MainController extends Controller{
	public function home(){

		$products = Product::latest()->simplePaginate(1);

		return view('main.home',["products" => $products]);
	}
}