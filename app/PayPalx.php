<?php

namespace App;

class Paypal
{
	private $_apiContext;
	private $shopping_cart;
	private $_ClientId = 'AUPt2-N-9NPO0b3M670KSyXA3gDYmcv_-neRCbFV2nq5R85ZeJ7suw7kcd0CEM5igRuP_jacUSgcTDVJ';
	private $_ClientSecret = 'EOsR0FEQEztx8xCfSSXniiR42YQ_3BlnNsPD2OWFOUpojU-S-UCE5KVDigOd5nJitY2yXRSo7MKQUdYp';

	public function _construct($shopping_cart){

		$this->_apiContext = \PaypalPayment::ApiContext($this->_ClientId, $this->_ClientSecret);

		$config = config("paypal_payment");
		$flatConfig = array_dot($config);

		$this->_apiContext->setConfig($flatConfig);

		$this->shopping_cart = $shopping_cart;
	}

	public function generate(){
		$payment = \PaypalPayment::payment()->setIntent("sale")
		                    ->setPayer($this->payer())
		                    ->setTransactions([$this->transaction()])
		                    ->setRedirectUrls($this->redirectURLs());
		try{
			$payment->create($this->_apiContext);
		}catch(\Exception $ex){
			dd($ex);
			exit(1);
		}

		return $payment;
	}

	public function payer(){
		//return patments´s info
		return \PaypalPayment::payer()->setPaymentMethod("paypal");
	}

	public function redirectURLs(){
		//Return transaction info´s
		$baseURL = url('url');
		return \PaypalPayment::redirectUrls()
		                     ->setReturnUrl("$baseURL/payments/store")
		                     ->setCancelUrl("$baseURL/carrito");
	}

	public function transaction(){
		//returns trasaction´s info
		return \PaypalPayment::transaction()
				->setAmount($this->amount())
				->setItemList($this->items())
				->setDescription("Tu compra en ComicStore")
				->setInvoiceNumber(uniqid());
	}

	public function items(){
		$items = [];

		$products = $this->shopping_cart->products()->get();

		foreach ($products as $product) {
			array_push($items, $product->paypalItem());
		}

		return \PaypalPayment::itemList()->setItems($items);
	}

	public function amount(){
		return \PaypalPayment::amount()->setCurrency("USD")->setTotal($this->shopping_cart->total());
	}

}