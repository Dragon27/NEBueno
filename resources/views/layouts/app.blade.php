<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/material-fullpalette.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/ripples.min.css">

    <link href="{{ url('/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        ComicStore
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li>
                            <a href="{{url('/carrito')}}">
                                Mi carrito
                                <span class="circle-shopping-cart">
                                    {{$productsCount}}
                                </span>
                            </a>
                        </li>
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" 
                                    style="display:none;">
                                    {{ csrf_field() }}
                                </form>        
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row" id="menu-sec">

                <div class="col-sm-12 col-sm-offset-3 visible-sm visible-md col-md-8 visible-lg">
                <a href="{{ url('/') }}" class="btn btn-primary"> ComicStore </a>
                <a href="{{ url('/quienessomos') }}" class="btn btn-primary"> Quienes Somos </a>
                <a href="{{ url('/ubicacion') }}" class="btn btn-primary"> Ubicacion </a>
                <a href="{{ url('/redessociales') }}" class="btn btn-primary"> Redes Sociales </a>
                <br><p></p>
                </div>

                <div class="col-xs-12 visible-xs">

                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">

                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only"> Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li>
                                    <a href="{{ url('/') }}" class="btn btn-primary"> ComicStore </a>
                                    </li>
                                    <li>
                                    <a href="{{ url('/catalogo') }}" class="btn btn-primary"> Catalogos </a>
                                    </li>
                                    <li>
                                    <a href="{{ url('/lanzamientos') }}" class="btn btn-primary"> Proximos Lanzamientos </a>
                                    </li>
                                    <li>
                                    <a href="{{ url('/promociones') }}" class="btn btn-primary"> Promociones </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </nav>
                    <br /><p></p>   
                </div>
            </div>

            @yield('content')

        </div>

        <div class="container" id="sha">

            <div class="row">
                <div class="col-xs-12">
                    <br />
                    <h2><p class="text-center">Contacto</p></h2>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-2">
                    <center><img src="img/mail.png" class="img-responsive"></center>    
                </div>
                <div class="col-xs-12 col-sm-2">
                    <br />
                    <strong>Contactanos via Correo <br /></strong>
                    <small><p>Disponible para cualquier duda o aclaracion <br /> <a href="#"> Quejas o sugerencias</a></p></small>  
                </div>

                <div class="col-xs-12 col-sm-2">
                    <center><img src="img/tel.png" class="img-responsive"></center> 
                </div>
                <div class="col-xs-12 col-sm-2">
                    <br />
                    <strong>Contactanos via Telefono <br /></strong>
                    <small><p>Para inforamcion de preventas <br /><a href="#"> o Dudas con tus compras</a></p></small>  
                </div>

                <div class="col-xs-12 col-sm-2">
                    <center><img src="img/social.png" class="img-responsive"></center>  
                </div>
                <div class="col-xs-12 col-sm-2">
                    <br />
                    <strong>Siguenos en nuestras redes sociales <br /></strong>
                    <small><p>Mantene informado de nuestras promociones <br /><a href="#"> Aprovecha nuestras promociones</a></p></small>   
                </div>
            </div>
        </div>

        <div class="container-fluid well">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <br />
                    <small class="pull-left">
                        <a href="#">Nuestras Politicas</a>
                        <a href="#">Distribuidores</a>  
                    </small>

                    <a href="#" class="pull-left"><img src="img/comic.png" class="img-responsive"></a>
                    <br />
                </div>
            </div>  
        </div>

    </div>



    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/js/material.min.js"></script> 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/js/ripples.min.js"></script> 
    <script>
        $.material.init();
    </script>
    <script src="{{ url('/js/app.js') }}"></script>
</body>
</html>
