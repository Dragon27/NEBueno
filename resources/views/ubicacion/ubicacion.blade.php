@extends('layouts.app')

@section('title','Productos Facilito')

@section('content')
<div class="container">
	<div class="row">
		<h1>Ubicacion</h1>
			<div class="col-xs-12 col-sm-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d232.7529001360857!2d-89.62214810257197!3d21.03082925779532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1481124787853" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>	
			</div>
			<div class="col-xs-12 col-sm-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1ses-419!2smx!4v1481125133480!6m8!1m7!1sGICtYEBEMMXoFERkiLK4lg!2m2!1d21.03082925779532!2d-89.62214810257197!3f177.77884497042416!4f1.1177219840380417!5f0.7820865974627469" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
	</div>
</div>
	
@endsection